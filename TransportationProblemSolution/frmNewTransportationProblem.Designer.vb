﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmNewTransportationProblem
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.tboxFactoryQuantity = New System.Windows.Forms.TextBox()
        Me.tboxMineQuantity = New System.Windows.Forms.TextBox()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.lboxMines = New System.Windows.Forms.ListBox()
        Me.DeleteMine = New System.Windows.Forms.Button()
        Me.GroupBox3 = New System.Windows.Forms.GroupBox()
        Me.lboxFactories = New System.Windows.Forms.ListBox()
        Me.DeleteFactory = New System.Windows.Forms.Button()
        Me.gboxMineInformation = New System.Windows.Forms.GroupBox()
        Me.btnUpdateMine = New System.Windows.Forms.Button()
        Me.AddMine = New System.Windows.Forms.Button()
        Me.tboxMineProductQuantity = New System.Windows.Forms.TextBox()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.GroupBox5 = New System.Windows.Forms.GroupBox()
        Me.btnUpdateFactory = New System.Windows.Forms.Button()
        Me.AddFactory = New System.Windows.Forms.Button()
        Me.tboxFactoryProductQuantity = New System.Windows.Forms.TextBox()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.dgviewTransportationProblem = New System.Windows.Forms.DataGridView()
        Me.MineName = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        Me.GroupBox2.SuspendLayout()
        Me.GroupBox3.SuspendLayout()
        Me.gboxMineInformation.SuspendLayout()
        Me.GroupBox5.SuspendLayout()
        CType(Me.dgviewTransportationProblem, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.tboxFactoryQuantity)
        Me.GroupBox1.Controls.Add(Me.tboxMineQuantity)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox1.ForeColor = System.Drawing.Color.Green
        Me.GroupBox1.Location = New System.Drawing.Point(12, 56)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(180, 126)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Số Trạm"
        '
        'tboxFactoryQuantity
        '
        Me.tboxFactoryQuantity.Location = New System.Drawing.Point(100, 49)
        Me.tboxFactoryQuantity.Name = "tboxFactoryQuantity"
        Me.tboxFactoryQuantity.ReadOnly = True
        Me.tboxFactoryQuantity.Size = New System.Drawing.Size(74, 20)
        Me.tboxFactoryQuantity.TabIndex = 3
        '
        'tboxMineQuantity
        '
        Me.tboxMineQuantity.Location = New System.Drawing.Point(100, 23)
        Me.tboxMineQuantity.Name = "tboxMineQuantity"
        Me.tboxMineQuantity.ReadOnly = True
        Me.tboxMineQuantity.Size = New System.Drawing.Size(74, 20)
        Me.tboxMineQuantity.TabIndex = 2
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.ForeColor = System.Drawing.Color.Black
        Me.Label3.Location = New System.Drawing.Point(6, 52)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(84, 13)
        Me.Label3.TabIndex = 1
        Me.Label3.Text = "Số Trạm Thu:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.ForeColor = System.Drawing.Color.Black
        Me.Label2.Location = New System.Drawing.Point(6, 26)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(88, 13)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "Số Trạm Phát:"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Font = New System.Drawing.Font("Microsoft Sans Serif", 13.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label1.ForeColor = System.Drawing.Color.Blue
        Me.Label1.Location = New System.Drawing.Point(158, 22)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(292, 22)
        Me.Label1.TabIndex = 1
        Me.Label1.Text = "TẠO MỚI - BÀI TOÁN VẬN TẢI "
        '
        'GroupBox2
        '
        Me.GroupBox2.Controls.Add(Me.lboxMines)
        Me.GroupBox2.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox2.ForeColor = System.Drawing.Color.Green
        Me.GroupBox2.Location = New System.Drawing.Point(198, 56)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Size = New System.Drawing.Size(180, 126)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = False
        Me.GroupBox2.Text = "Trạm Phát"
        '
        'lboxMines
        '
        Me.lboxMines.FormattingEnabled = True
        Me.lboxMines.Location = New System.Drawing.Point(6, 19)
        Me.lboxMines.Name = "lboxMines"
        Me.lboxMines.Size = New System.Drawing.Size(168, 95)
        Me.lboxMines.TabIndex = 0
        '
        'DeleteMine
        '
        Me.DeleteMine.ForeColor = System.Drawing.Color.Black
        Me.DeleteMine.Location = New System.Drawing.Point(189, 49)
        Me.DeleteMine.Name = "DeleteMine"
        Me.DeleteMine.Size = New System.Drawing.Size(75, 23)
        Me.DeleteMine.TabIndex = 3
        Me.DeleteMine.Text = "Xóa"
        Me.DeleteMine.UseVisualStyleBackColor = True
        '
        'GroupBox3
        '
        Me.GroupBox3.Controls.Add(Me.lboxFactories)
        Me.GroupBox3.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox3.ForeColor = System.Drawing.Color.Green
        Me.GroupBox3.Location = New System.Drawing.Point(384, 56)
        Me.GroupBox3.Name = "GroupBox3"
        Me.GroupBox3.Size = New System.Drawing.Size(180, 126)
        Me.GroupBox3.TabIndex = 3
        Me.GroupBox3.TabStop = False
        Me.GroupBox3.Text = "Trạm Thu"
        '
        'lboxFactories
        '
        Me.lboxFactories.FormattingEnabled = True
        Me.lboxFactories.Location = New System.Drawing.Point(6, 19)
        Me.lboxFactories.Name = "lboxFactories"
        Me.lboxFactories.Size = New System.Drawing.Size(168, 95)
        Me.lboxFactories.TabIndex = 1
        '
        'DeleteFactory
        '
        Me.DeleteFactory.ForeColor = System.Drawing.Color.Black
        Me.DeleteFactory.Location = New System.Drawing.Point(189, 49)
        Me.DeleteFactory.Name = "DeleteFactory"
        Me.DeleteFactory.Size = New System.Drawing.Size(75, 23)
        Me.DeleteFactory.TabIndex = 4
        Me.DeleteFactory.Text = "Xóa"
        Me.DeleteFactory.UseVisualStyleBackColor = True
        '
        'gboxMineInformation
        '
        Me.gboxMineInformation.Controls.Add(Me.btnUpdateMine)
        Me.gboxMineInformation.Controls.Add(Me.DeleteMine)
        Me.gboxMineInformation.Controls.Add(Me.AddMine)
        Me.gboxMineInformation.Controls.Add(Me.tboxMineProductQuantity)
        Me.gboxMineInformation.Controls.Add(Me.Label4)
        Me.gboxMineInformation.Enabled = False
        Me.gboxMineInformation.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gboxMineInformation.ForeColor = System.Drawing.Color.Green
        Me.gboxMineInformation.Location = New System.Drawing.Point(12, 188)
        Me.gboxMineInformation.Name = "gboxMineInformation"
        Me.gboxMineInformation.Size = New System.Drawing.Size(270, 80)
        Me.gboxMineInformation.TabIndex = 4
        Me.gboxMineInformation.TabStop = False
        Me.gboxMineInformation.Text = "Thông tin Trạm Phát"
        '
        'btnUpdateMine
        '
        Me.btnUpdateMine.ForeColor = System.Drawing.Color.Black
        Me.btnUpdateMine.Location = New System.Drawing.Point(108, 49)
        Me.btnUpdateMine.Name = "btnUpdateMine"
        Me.btnUpdateMine.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdateMine.TabIndex = 4
        Me.btnUpdateMine.Text = "Thay đổi"
        Me.btnUpdateMine.UseVisualStyleBackColor = True
        '
        'AddMine
        '
        Me.AddMine.ForeColor = System.Drawing.Color.Black
        Me.AddMine.Location = New System.Drawing.Point(6, 49)
        Me.AddMine.Name = "AddMine"
        Me.AddMine.Size = New System.Drawing.Size(75, 23)
        Me.AddMine.TabIndex = 2
        Me.AddMine.Text = "Thêm"
        Me.AddMine.UseVisualStyleBackColor = True
        '
        'tboxMineProductQuantity
        '
        Me.tboxMineProductQuantity.ForeColor = System.Drawing.Color.Black
        Me.tboxMineProductQuantity.Location = New System.Drawing.Point(130, 23)
        Me.tboxMineProductQuantity.Name = "tboxMineProductQuantity"
        Me.tboxMineProductQuantity.Size = New System.Drawing.Size(134, 20)
        Me.tboxMineProductQuantity.TabIndex = 1
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label4.ForeColor = System.Drawing.Color.Black
        Me.Label4.Location = New System.Drawing.Point(6, 26)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(118, 13)
        Me.Label4.TabIndex = 0
        Me.Label4.Text = "Số lượng hàng hóa:"
        '
        'GroupBox5
        '
        Me.GroupBox5.Controls.Add(Me.btnUpdateFactory)
        Me.GroupBox5.Controls.Add(Me.DeleteFactory)
        Me.GroupBox5.Controls.Add(Me.AddFactory)
        Me.GroupBox5.Controls.Add(Me.tboxFactoryProductQuantity)
        Me.GroupBox5.Controls.Add(Me.Label5)
        Me.GroupBox5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.GroupBox5.ForeColor = System.Drawing.Color.Green
        Me.GroupBox5.Location = New System.Drawing.Point(294, 188)
        Me.GroupBox5.Name = "GroupBox5"
        Me.GroupBox5.Size = New System.Drawing.Size(270, 80)
        Me.GroupBox5.TabIndex = 5
        Me.GroupBox5.TabStop = False
        Me.GroupBox5.Text = "Thông tin Trạm Thu"
        '
        'btnUpdateFactory
        '
        Me.btnUpdateFactory.ForeColor = System.Drawing.Color.Black
        Me.btnUpdateFactory.Location = New System.Drawing.Point(108, 49)
        Me.btnUpdateFactory.Name = "btnUpdateFactory"
        Me.btnUpdateFactory.Size = New System.Drawing.Size(75, 23)
        Me.btnUpdateFactory.TabIndex = 5
        Me.btnUpdateFactory.Text = "Thay đổi"
        Me.btnUpdateFactory.UseVisualStyleBackColor = True
        '
        'AddFactory
        '
        Me.AddFactory.ForeColor = System.Drawing.Color.Black
        Me.AddFactory.Location = New System.Drawing.Point(6, 49)
        Me.AddFactory.Name = "AddFactory"
        Me.AddFactory.Size = New System.Drawing.Size(75, 23)
        Me.AddFactory.TabIndex = 3
        Me.AddFactory.Text = "Thêm"
        Me.AddFactory.UseVisualStyleBackColor = True
        '
        'tboxFactoryProductQuantity
        '
        Me.tboxFactoryProductQuantity.ForeColor = System.Drawing.Color.Black
        Me.tboxFactoryProductQuantity.Location = New System.Drawing.Point(130, 23)
        Me.tboxFactoryProductQuantity.Name = "tboxFactoryProductQuantity"
        Me.tboxFactoryProductQuantity.Size = New System.Drawing.Size(134, 20)
        Me.tboxFactoryProductQuantity.TabIndex = 3
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Label5.ForeColor = System.Drawing.Color.Black
        Me.Label5.Location = New System.Drawing.Point(6, 26)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(118, 13)
        Me.Label5.TabIndex = 2
        Me.Label5.Text = "Số lượng hàng hóa:"
        '
        'dgviewTransportationProblem
        '
        Me.dgviewTransportationProblem.AllowUserToAddRows = False
        Me.dgviewTransportationProblem.AllowUserToDeleteRows = False
        Me.dgviewTransportationProblem.AllowUserToResizeColumns = False
        Me.dgviewTransportationProblem.AllowUserToResizeRows = False
        Me.dgviewTransportationProblem.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.dgviewTransportationProblem.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.MineName})
        Me.dgviewTransportationProblem.Location = New System.Drawing.Point(12, 274)
        Me.dgviewTransportationProblem.MultiSelect = False
        Me.dgviewTransportationProblem.Name = "dgviewTransportationProblem"
        Me.dgviewTransportationProblem.RowHeadersWidth = 60
        Me.dgviewTransportationProblem.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.CellSelect
        Me.dgviewTransportationProblem.ShowEditingIcon = False
        Me.dgviewTransportationProblem.Size = New System.Drawing.Size(552, 185)
        Me.dgviewTransportationProblem.TabIndex = 6
        '
        'MineName
        '
        Me.MineName.HeaderText = "Mine Name"
        Me.MineName.Name = "MineName"
        '
        'frmNewTransportationProblem
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(586, 485)
        Me.Controls.Add(Me.dgviewTransportationProblem)
        Me.Controls.Add(Me.GroupBox5)
        Me.Controls.Add(Me.gboxMineInformation)
        Me.Controls.Add(Me.GroupBox3)
        Me.Controls.Add(Me.GroupBox2)
        Me.Controls.Add(Me.Label1)
        Me.Controls.Add(Me.GroupBox1)
        Me.ForeColor = System.Drawing.Color.Green
        Me.Name = "frmNewTransportationProblem"
        Me.Text = "Bài toán Vận tải - Tạo Mới"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.GroupBox2.ResumeLayout(False)
        Me.GroupBox3.ResumeLayout(False)
        Me.gboxMineInformation.ResumeLayout(False)
        Me.gboxMineInformation.PerformLayout()
        Me.GroupBox5.ResumeLayout(False)
        Me.GroupBox5.PerformLayout()
        CType(Me.dgviewTransportationProblem, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents Label3 As Label
    Friend WithEvents Label2 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents GroupBox3 As GroupBox
    Friend WithEvents DeleteMine As Button
    Friend WithEvents DeleteFactory As Button
    Friend WithEvents gboxMineInformation As GroupBox
    Friend WithEvents AddMine As Button
    Friend WithEvents tboxMineProductQuantity As TextBox
    Friend WithEvents Label4 As Label
    Friend WithEvents GroupBox5 As GroupBox
    Friend WithEvents AddFactory As Button
    Friend WithEvents tboxFactoryProductQuantity As TextBox
    Friend WithEvents Label5 As Label
    Friend WithEvents tboxFactoryQuantity As TextBox
    Friend WithEvents tboxMineQuantity As TextBox
    Friend WithEvents dgviewTransportationProblem As DataGridView
    Friend WithEvents btnUpdateMine As Button
    Friend WithEvents btnUpdateFactory As Button
    Friend WithEvents MineName As DataGridViewTextBoxColumn
    Friend WithEvents lboxMines As ListBox
    Friend WithEvents lboxFactories As ListBox
End Class
