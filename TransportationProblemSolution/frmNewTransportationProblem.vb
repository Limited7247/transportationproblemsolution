﻿Public Class frmNewTransportationProblem

    Private Sub frmNewTransportationProblem_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        tboxMineQuantity.Text = "0"
        tboxFactoryQuantity.Text = "0"

    End Sub

    'Yêu cầu có tối thiểu 1 Trạm thu trước khi thêm Trạm phát
    Private Sub tboxFactoryQuantity_TextChanged(sender As Object, e As EventArgs) Handles tboxFactoryQuantity.TextChanged
        If Integer.Parse(tboxFactoryQuantity.Text) > 0 Then
            gboxMineInformation.Enabled = True
        Else
            gboxMineInformation.Enabled = False
        End If
    End Sub

    'Event thay đổi lựa chọn Trạm phát
    Private Sub lboxMines_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lboxMines.SelectedIndexChanged
        UpdateMineProductQuantity()
    End Sub

    Private Sub UpdateMineProductQuantity()
        If lboxMines.SelectedIndex <> -1 Then
            tboxMineProductQuantity.Text = lboxMines.SelectedItem.ToString
        Else
            tboxMineProductQuantity.Clear()
        End If

        tboxMineProductQuantity.Focus()
    End Sub

    'Event thay đổi lựa chọn Trạm thu
    Private Sub lboxFactories_SelectedIndexChanged(sender As Object, e As EventArgs) Handles lboxFactories.SelectedIndexChanged
        UpdateFactoryProductQuantity()
    End Sub

    Private Sub UpdateFactoryProductQuantity()
        If lboxFactories.SelectedIndex <> -1 Then
            tboxFactoryProductQuantity.Text = lboxFactories.SelectedItem.ToString
        Else
            tboxFactoryProductQuantity.Clear()
        End If

        tboxFactoryProductQuantity.Focus()
    End Sub

    'Thêm Trạm phát
    Private Sub Mine_Add(sender As Object, e As EventArgs) Handles AddMine.Click
        Dim MineQuantity As Integer

        If Integer.TryParse(tboxMineProductQuantity.Text, MineQuantity) Then
            lboxMines.Items.Add(MineQuantity)

            'Thêm dòng vào Bảng Vận tải
            dgviewTransportationProblem.Rows.Add(String.Format("M{0}", lboxMines.Items.Count), MineQuantity.ToString)
            'Thêm giá trị Trạm phát vào Bảng Vận tải
            dgviewTransportationProblem.Rows.Item(dgviewTransportationProblem.Rows.Count - 1).HeaderCell.Value = MineQuantity.ToString

            tboxMineQuantity.Text = lboxMines.Items.Count().ToString 'Recount Số lượng Trạm phát
            UpdateMineProductQuantity() 'Cập nhật Số lượng hàng hóa Trạm phát

        Else
            MessageBox.Show("Mời nhập lại Số lượng hàng hóa Trạm phát", "Thêm Trạm phát", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

    End Sub

    'Thay đổi lượng hàng hóa của Trạm phát
    Private Sub btnUpdateMine_Click(sender As Object, e As EventArgs) Handles btnUpdateMine.Click
        If lboxMines.SelectedIndex <> -1 Then
            Dim MineQuantity As Integer

            If Integer.TryParse(tboxMineProductQuantity.Text, MineQuantity) Then
                lboxMines.Items(lboxMines.SelectedIndex) = MineQuantity.ToString
                UpdateMineProductQuantity()

                'Sửa dòng trên Bảng Vận tải
                dgviewTransportationProblem.Rows.Item(lboxMines.SelectedIndex).Cells(0).Value = String.Format("M{0}", lboxMines.SelectedIndex + 1)
                dgviewTransportationProblem.Rows.Item(lboxMines.SelectedIndex).Cells(1).Value = MineQuantity.ToString
                'Sửa giá trị Trạm phát trên Bảng Vận tải
                dgviewTransportationProblem.Rows.Item(lboxMines.SelectedIndex).HeaderCell.Value = MineQuantity.ToString
            End If

        Else
            MessageBox.Show("Bạn cần chọn Trạm phát!", "Thay đổi Số lượng hàng hóa Trạm phát", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    'Loại bỏ Trạm phát
    Private Sub DeleteMine_Click(sender As Object, e As EventArgs) Handles DeleteMine.Click
        If lboxMines.SelectedIndex <> -1 Then
            lboxMines.Items.RemoveAt(lboxMines.SelectedIndex)
            tboxMineQuantity.Text = lboxMines.Items.Count().ToString

            UpdateMineProductQuantity()
        Else
            MessageBox.Show("Bạn cần chọn Trạm phát!", "Xóa Trạm phát", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    'Thêm Trạm thu
    Private Sub Factory_Add(sender As Object, e As EventArgs) Handles AddFactory.Click
        Dim FactoryQuantity As Integer

        If Integer.TryParse(tboxFactoryProductQuantity.Text, FactoryQuantity) Then
            lboxFactories.Items.Add(FactoryQuantity.ToString)
            tboxFactoryQuantity.Text = lboxFactories.Items.Count().ToString

            UpdateFactoryProductQuantity()

            dgviewTransportationProblem.Columns.Add(String.Format("F{0}", FactoryQuantity), FactoryQuantity.ToString)

        Else
            MessageBox.Show("Mời nhập lại Số lượng hàng hóa Trạm phát!", "Thêm Trạm phát", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If

    End Sub

    'Thay đổi số lượng hàng hóa Trạm thu
    Private Sub btnUpdateFactory_Click(sender As Object, e As EventArgs) Handles btnUpdateFactory.Click
        If lboxFactories.SelectedIndex <> -1 Then
            Dim FactoryQuantity As Integer

            If Integer.TryParse(tboxFactoryProductQuantity.Text, FactoryQuantity) Then
                lboxFactories.Items(lboxFactories.SelectedIndex) = FactoryQuantity
                UpdateFactoryProductQuantity()

                'Sửa giá trị Trạm thu trên Bảng Vận tải
                dgviewTransportationProblem.Columns(lboxFactories.SelectedIndex + 1).HeaderText = FactoryQuantity.ToString
            End If

        Else
            MessageBox.Show("Bạn cần chọn Trạm thu!", "Thay đổi Số lượng hàng hóa Trạm thu", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub

    'Loại bỏ Trạm thu
    Private Sub DeleteFactory_Click(sender As Object, e As EventArgs) Handles DeleteFactory.Click
        If lboxFactories.SelectedIndex <> -1 Then
            lboxFactories.Items.RemoveAt(lboxFactories.SelectedIndex)
            tboxFactoryQuantity.Text = lboxFactories.Items.Count.ToString

            UpdateFactoryProductQuantity()
        Else
            MessageBox.Show("Bạn cần chọn Trạm thu!", "Xóa Trạm thu", MessageBoxButtons.OK, MessageBoxIcon.Warning)
        End If
    End Sub


End Class